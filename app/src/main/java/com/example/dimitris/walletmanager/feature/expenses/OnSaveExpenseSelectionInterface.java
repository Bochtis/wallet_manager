package com.example.dimitris.walletmanager.feature.expenses;

public interface OnSaveExpenseSelectionInterface {
    void onSaveExpenseItemSelected();
}
