package com.example.dimitris.walletmanager.data.db;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "budget_table")
public class BudgetEntry implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String category;
    private String value;

    @Ignore
    public BudgetEntry(String category, String value) {
        this.category = category;
        this.value = value;
    }

    public BudgetEntry(int id, String category, String value) {
        this.id = id;
        this.category = category;
        this.value = value;
    }

    protected BudgetEntry(Parcel in) {
        id = in.readInt();
        category = in.readString();
        value = in.readString();
    }

    public static final Creator<BudgetEntry> CREATOR = new Creator<BudgetEntry>() {
        @Override
        public BudgetEntry createFromParcel(Parcel in) {
            return new BudgetEntry(in);
        }

        @Override
        public BudgetEntry[] newArray(int size) {
            return new BudgetEntry[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(category);
        dest.writeString(value);
    }
}
