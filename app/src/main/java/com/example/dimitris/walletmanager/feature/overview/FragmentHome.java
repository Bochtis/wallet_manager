package com.example.dimitris.walletmanager.feature.overview;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.data.prefs.PreferencesHelper;
import com.example.dimitris.walletmanager.viewmodel.MyViewModel;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static com.example.dimitris.walletmanager.utils.Utils.formatValue;

public class FragmentHome extends Fragment {

    private PieChart chart;
    private TextView balanceTxtView, currentBalanceTxtView, totalExpensesTxtView, dateTxtView, remainingDays;

    private double currentBalance = 0.0, totalExpenses = 0.0;
    private int restDaysOfMonth;
    private SimpleDateFormat formatter;
    private PreferencesHelper prefs;
    private MyViewModel mMyViewModel;

    public FragmentHome() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        setupViews(view);
        setupViewModel();

        return view;
    }

    private void setupViews(View view) {
        chart = view.findViewById(R.id.chart);
        balanceTxtView = view.findViewById(R.id.balance_txt_view);
        currentBalanceTxtView = view.findViewById(R.id.current_balance_txt_view);
        totalExpensesTxtView = view.findViewById(R.id.total_expenses_txt_view);
        dateTxtView = view.findViewById(R.id.date_txt_view);
        remainingDays = view.findViewById(R.id.remaining_days_txt_view);

        prefs = new PreferencesHelper(Objects.requireNonNull(getContext()));
        balanceTxtView.setText(formatValue(prefs.getBudget()));

        formatter = new SimpleDateFormat("EEE, MMM d, ''yy", Locale.ENGLISH);

        setupChart();
    }

    private void setupChart() {
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setCenterTextTypeface(Typeface.SANS_SERIF);
        chart.setCenterTextSize(22);
        chart.setCenterTextColor(Objects.requireNonNull(getContext()).getResources().getColor(R.color.fragment_home_primary_text));
//        chart.setCenterText("Transaction \nRecords");

        chart.setHoleColor(Color.WHITE);

        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        chart.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setYOffset(10f);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTypeface(Typeface.SANS_SERIF);
        chart.setEntryLabelTextSize(14f);
        chart.setDrawEntryLabels(false);
    }

    private void setupViewModel() {
        // Get a new or existing ViewModel from the ViewModelProvider.
        mMyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MyViewModel.class);

        // Check if the month has changed
        if (prefs.getMonthChangedStatus()) {
            clearWallet();
        }

        // Add an observer on the LiveData returned by getExpenses.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mMyViewModel.getExpenses().observe(this, new Observer<List<ExpenseEntry>>() {
            @Override
            public void onChanged(@Nullable final List<ExpenseEntry> expenses) {
                if (expenses != null) {
                    setData(expenses);

                    totalExpenses = getTotalExpenses(expenses);
                    totalExpensesTxtView.setText(formatValue(String.valueOf(totalExpenses)));

                    currentBalance = Double.valueOf(prefs.getBudget()) - totalExpenses;
                    currentBalanceTxtView.setText(formatValue(String.valueOf(currentBalance)));

                    Calendar calendar = Calendar.getInstance();
                    Date mDate = calendar.getTime();
                    dateTxtView.setText(formatter.format(mDate));

                    restDaysOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH) - calendar.get(Calendar.DAY_OF_MONTH);
                    if (restDaysOfMonth == 0)
                        remainingDays.setText(Objects.requireNonNull(getContext()).getString(R.string.fragment_home_last_day_of_month));
                    else if (restDaysOfMonth == 1)
                        remainingDays.setText(Objects.requireNonNull(getContext()).getString(R.string.fragment_home_remaining_day));
                    else
                        remainingDays.setText(String.format(Objects.requireNonNull(getContext()).getString(R.string.fragment_home_remaining_days), restDaysOfMonth));
                }
            }
        });
    }

    private void clearWallet() {
        mMyViewModel.clearExpenses();
        // ask user to clear the budgets too
        showDialog();
    }

    private void showDialog() {
        new MaterialAlertDialogBuilder(Objects.requireNonNull(getContext()))
                .setTitle(getContext().getString(R.string.fragment_home_dialog_title))
                .setMessage(getContext().getString(R.string.fragment_home_dialog_description))
                .setPositiveButton(getContext().getString(R.string.fragment_home_dialog_positive_btn), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearBudgets();
                        clearPieChart();
                        mMyViewModel.clearBudgets();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getContext().getString(R.string.fragment_home_dialog_negative_btn), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .show();
    }

    private void clearBudgets() {
        prefs.setBudget("0.0");
        balanceTxtView.setText(formatValue("0.0"));
        currentBalanceTxtView.setText(formatValue("0.0"));
    }

    private void clearPieChart() {
        setData(null);
    }

    private double getTotalExpenses(List<ExpenseEntry> expenses) {
        double totalExpenses = 0.0;

        for (ExpenseEntry expenseEntry : expenses) {
            totalExpenses += Double.valueOf(expenseEntry.getValue());
        }

        return totalExpenses;
    }

    private void setData(List<ExpenseEntry> expenseEntries) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        PieEntry budgetEntry = new PieEntry(100, "Budget");
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(budgetEntry);
        if (!prefs.getBudget().isEmpty() && !prefs.getBudget().equals("0.0")) {
            if (expenseEntries != null && expenseEntries.size() > 0) {
                HashMap<String, Double> expenseCategories = defineExpenseCategories(expenseEntries);
                double budget = Double.valueOf(prefs.getBudget());
                for (Map.Entry<String, Double> entry : expenseCategories.entrySet()) {
                    double expense = entry.getValue();
                    float expensePercentage = (float) (expense * 100 / budget);
                    entries.add(new PieEntry(expensePercentage,
                            entry.getKey()));
                }
            }
        }

        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);
        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setValueTextSize(12f);
        dataSet.setSelectionShift(5f);

        // add colors
        ArrayList<Integer> colors = prefs.getColors();
        if (colors == null || colors.size() == 0) {
            colors = new ArrayList<>();
            // empty budget and expenses
            colors.add(getRandomColor());
        } else {
            if (expenseEntries != null) {
                // have expenses
                int newColorsCount = expenseEntries.size() - (colors.size() - 1);
                if (newColorsCount > 0) {
                    for (int i = 0; i < newColorsCount; i++) {
                        colors.add(getRandomColor());
                    }
                }
            }
        }

        prefs.addColors(colors);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(Typeface.SANS_SERIF);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }

    private HashMap<String, Double> defineExpenseCategories(List<ExpenseEntry> expenseEntries) {
        HashMap<String, Double> expenseCategories = new HashMap<>();
        for (ExpenseEntry expenseEntry : expenseEntries) {
            if (expenseCategories.containsKey(expenseEntry.getCategory())) {
                //noinspection ConstantConditions
                expenseCategories.put(expenseEntry.getCategory(), expenseCategories.get(expenseEntry.getCategory()) + Double.valueOf(expenseEntry.getValue()));
            } else {
                expenseCategories.put(expenseEntry.getCategory(), Double.valueOf(expenseEntry.getValue()));
            }
        }
        return expenseCategories;
    }

    private int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
}
