package com.example.dimitris.walletmanager.data.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MyDao {

    // expense methods
    @Query("SELECT * FROM expense_table")
    LiveData<List<ExpenseEntry>> getExpenses();

    @Insert
    void insertExpense(ExpenseEntry expenseEntry);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateExpense(ExpenseEntry expenseEntry);

    @Delete
    void deleteExpense(ExpenseEntry expenseEntry);

    @Query("DELETE FROM expense_table")
    void clearExpenses();

    // budget methods
    @Query("SELECT * FROM budget_table")
    LiveData<List<BudgetEntry>> getBudgets();

    @Insert
    void insertBudget(BudgetEntry budgetEntry);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateBudget(BudgetEntry budgetEntry);

    @Delete
    void deleteBudget(BudgetEntry budgetEntry);

    @Query("DELETE FROM budget_table")
    void clearBudgets();
}
