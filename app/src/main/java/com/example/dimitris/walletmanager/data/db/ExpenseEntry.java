package com.example.dimitris.walletmanager.data.db;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "expense_table")
public class ExpenseEntry implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String category;
    private String description;
    private String value;
    private String date;
    @ColumnInfo(name = "icon_id")
    private int iconId;

    @Ignore
    public ExpenseEntry(String category, String description, String value, int iconId, String date) {
        this.category = category;
        this.description = description;
        this.value = value;
        this.iconId = iconId;
        this.date = date;
    }

    public ExpenseEntry(int id, String category, String description, String value, int iconId, String date) {
        this.id = id;
        this.category = category;
        this.description = description;
        this.value = value;
        this.iconId = iconId;
        this.date = date;
    }

    protected ExpenseEntry(Parcel in) {
        id = in.readInt();
        category = in.readString();
        description = in.readString();
        value = in.readString();
        date = in.readString();
        iconId = in.readInt();
    }

    public static final Creator<ExpenseEntry> CREATOR = new Creator<ExpenseEntry>() {
        @Override
        public ExpenseEntry createFromParcel(Parcel in) {
            return new ExpenseEntry(in);
        }

        @Override
        public ExpenseEntry[] newArray(int size) {
            return new ExpenseEntry[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(category);
        parcel.writeString(description);
        parcel.writeString(value);
        parcel.writeString(date);
        parcel.writeInt(iconId);
    }
}
