package com.example.dimitris.walletmanager.utils;

public class Extras {

    public static final String FIRST_LAUNCH = "first_launch";
    public static final String TIME = "time";
    public static final String MONTH_CHANGED = "month_changed";
    public static final String EXPENSE = "expense";
    public static final String BUDGET = "budget";
    public static final String COLORS = "colors";
    public static final String PREFERENCES = "my_prefs" ;

}
