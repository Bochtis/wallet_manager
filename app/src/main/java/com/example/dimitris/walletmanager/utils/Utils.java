package com.example.dimitris.walletmanager.utils;

import java.text.DecimalFormat;

public class Utils {

    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static String formatValue(String value) {
        return "€" + decimalFormat.format(Double.valueOf(value));
    }

    public static String toCamelCase(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        return text.substring(0,1).toUpperCase() + text.substring(1).toLowerCase();
    }
}
