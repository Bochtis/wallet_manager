package com.example.dimitris.walletmanager.feature.budget;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.utils.Extras;
import com.example.dimitris.walletmanager.viewmodel.MyViewModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class FragmentNewBudget extends DialogFragment {

    private BudgetEntry budgetEntry;
    private AutoCompleteTextView categoryAutoCompleteTxtView;
    private EditText valueEditTxt;
    private Button doneBtn;

    private String category, value;
    private BudgetEntry newBudgetEntry;
    private MyViewModel mMyViewModel;
    private ArrayList<String> categories = new ArrayList<>();
    private ArrayList<String> filteredCategories = new ArrayList<>();
    private Set<String> setOfCategories = new HashSet<>();
    private ArrayAdapter adapter;

    public FragmentNewBudget() {

    }

    public static FragmentNewBudget newInstance(BudgetEntry budgetEntry) {
        FragmentNewBudget fragmentNewBudget = new FragmentNewBudget();
        Bundle args = new Bundle();
        args.putParcelable(Extras.BUDGET, budgetEntry);
        fragmentNewBudget.setArguments(args);
        return fragmentNewBudget;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            budgetEntry = args.getParcelable(Extras.BUDGET);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_budget, container, false);

        setupViews(view);
        setupViewModel();

        return view;
    }

    private void setupViews(View view) {
        categoryAutoCompleteTxtView = view.findViewById(R.id.category_auto_complete_txt_view);
        valueEditTxt = view.findViewById(R.id.value_edit_txt);
        doneBtn = view.findViewById(R.id.done_btn);

        // fill in the edit text fields with the budget data
        if (budgetEntry != null) {
            categoryAutoCompleteTxtView.setText(budgetEntry.getCategory());
            valueEditTxt.setText(budgetEntry.getValue());
        }

        // Create the adapter and set it to the AutoCompleteTextView
        adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, filteredCategories);
        categoryAutoCompleteTxtView.setAdapter(adapter);

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category = categoryAutoCompleteTxtView.getText().toString();
                value = valueEditTxt.getText().toString();
                if (fieldsAreFilled()) {
                    if (budgetEntry != null) {
                        // update budget to database
                        budgetEntry.setCategory(category);
                        budgetEntry.setValue(value);
                        mMyViewModel.updateBudget(budgetEntry);
                    } else {
                        // add new budget to database
                        newBudgetEntry = new BudgetEntry(category, value);
                        mMyViewModel.insertBudget(newBudgetEntry);
                    }
                    dismiss();
                }
            }
        });
    }

    private Boolean fieldsAreFilled() {
        if (category.isEmpty()) {
            Toast.makeText(getContext(), "Please set a category name first..", Toast.LENGTH_SHORT).show();
            return false;
        } else if (value.isEmpty()) {
            Toast.makeText(getContext(), "Please set a value..", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void setupViewModel() {
        // Get a new or existing ViewModel from the ViewModelProvider.
        mMyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MyViewModel.class);

        // Observe the expense entries and update the adapter
        mMyViewModel.getExpenses().observe(this, new Observer<List<ExpenseEntry>>() {
            @Override
            public void onChanged(@Nullable List<ExpenseEntry> expenseEntries) {
                if (expenseEntries != null)
                    for (int i = 0; i < expenseEntries.size(); i++) {
                        categories.add(expenseEntries.get(i).getCategory());
                    }
                // remove duplicates
                setOfCategories.addAll(categories);
                filteredCategories.addAll(setOfCategories);
                adapter.notifyDataSetChanged();
            }
        });
    }
}
