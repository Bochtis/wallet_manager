package com.example.dimitris.walletmanager.feature.expenses;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.utils.RecyclerItemTouchHelper;
import com.example.dimitris.walletmanager.viewmodel.MyViewModel;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;
import java.util.Objects;

public class FragmentExpenses extends Fragment implements ExpensesRecyclerAdapter.ExpensesInterface,
        RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private TextView emptyTxtView;
    private RecyclerView expensesRecyclerView;

    private OnAddExpenseSelectionInterface onAddExpenseSelectionInterface;
    private OnExpenseItemSelectionInterface onExpenseItemSelectionInterface;
    private ExpensesRecyclerAdapter adapter;
    private MyViewModel mMyViewModel;

    public FragmentExpenses() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expenses, container, false);

        setupViews(view);
        setupViewModel();

        return view;
    }

    private void setupViews(View view) {
        emptyTxtView = view.findViewById(R.id.empty_txt_view);
        expensesRecyclerView = view.findViewById(R.id.expenses_recycler_view);
        FloatingActionButton newExpenseBtn = view.findViewById(R.id.new_expense_btn);
        newExpenseBtn.attachToRecyclerView(expensesRecyclerView);
        newExpenseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddExpenseSelectionInterface.onAddExpenseItemSelected();
            }
        });

        setupRecycler();
    }

    private void setupRecycler() {
        adapter = new ExpensesRecyclerAdapter(this);
        expensesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        expensesRecyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getContext()), DividerItemDecoration.VERTICAL));
        expensesRecyclerView.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback callback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(callback).attachToRecyclerView(expensesRecyclerView);
    }

    private void setupViewModel() {
        // Get a new or existing ViewModel from the ViewModelProvider.
        mMyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MyViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mMyViewModel.getExpenses().observe(this, new Observer<List<ExpenseEntry>>() {
            @Override
            public void onChanged(@Nullable final List<ExpenseEntry> expenses) {
                // Update the cached copy of the words in the adapter.
                adapter.setExpenses(expenses);

                if (expenses != null && expenses.size() > 0) {
                    emptyTxtView.setVisibility(View.GONE);
                    expensesRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    emptyTxtView.setVisibility(View.VISIBLE);
                    expensesRecyclerView.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddExpenseSelectionInterface) {
            onAddExpenseSelectionInterface = (OnAddExpenseSelectionInterface) context;
        }
        if (context instanceof OnExpenseItemSelectionInterface) {
            onExpenseItemSelectionInterface = (OnExpenseItemSelectionInterface) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onAddExpenseSelectionInterface = null;
        onExpenseItemSelectionInterface = null;
    }

    @Override
    public void onExpenseItemClicked(ExpenseEntry item) {
        //noinspection unchecked
        onExpenseItemSelectionInterface.onExpenseItemSelected(item);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ExpensesRecyclerAdapter.MyViewHolder) {
            //get the expense that will be deleted
            ExpenseEntry expenseEntry = adapter.getExpense(viewHolder.getAdapterPosition());
            // remove the item from recycler view
            adapter.removeExpense(viewHolder.getAdapterPosition());
            // delete the item from database
            mMyViewModel.deleteExpense(expenseEntry);
        }
    }
}

