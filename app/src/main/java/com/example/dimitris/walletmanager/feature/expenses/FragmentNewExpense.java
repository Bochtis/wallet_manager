package com.example.dimitris.walletmanager.feature.expenses;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.utils.Extras;
import com.example.dimitris.walletmanager.viewmodel.MyViewModel;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;
import com.maltaisn.icondialog.IconView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

public class FragmentNewExpense extends Fragment implements View.OnClickListener, IconDialog.Callback {

    private AutoCompleteTextView categoryAutoCompleteTxtView;
    private EditText descriptionEditTxt;
    private EditText valueEditTxt;
    private IconView iconView;
    private TextView dateValueTxtView;
    private Button doneBtn;

    private ExpenseEntry expenseEntry, newExpenseEntry;
    private String category, description, value, date;
    private DateFormat formatter;
    private OnSaveExpenseSelectionInterface onSaveExpenseSelectionInterface;
    private int selectedIconId = -1;
    private ArrayList<String> categories = new ArrayList<>();
    private ArrayList<String> filteredCategories = new ArrayList<>();
    private Set<String> setOfCategories = new HashSet<>();
    private ArrayAdapter adapter;

    private MyViewModel mMyViewModel;

    public FragmentNewExpense() {
    }

    public static FragmentNewExpense newInstance(ExpenseEntry expenseEntry) {
        FragmentNewExpense fragmentNewExpense = new FragmentNewExpense();
        Bundle args = new Bundle();
        args.putParcelable(Extras.EXPENSE, expenseEntry);
        fragmentNewExpense.setArguments(args);
        return fragmentNewExpense;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            expenseEntry = args.getParcelable(Extras.EXPENSE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_expense, container, false);

        setupViews(view);
        setupViewModel();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSaveExpenseSelectionInterface) {
            onSaveExpenseSelectionInterface = (OnSaveExpenseSelectionInterface) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onSaveExpenseSelectionInterface = null;
    }

    private void setupViews(View view) {
        categoryAutoCompleteTxtView = view.findViewById(R.id.category_auto_complete_txt_view);
        descriptionEditTxt = view.findViewById(R.id.description_edit_txt);
        valueEditTxt = view.findViewById(R.id.value_edit_txt);
        iconView = view.findViewById(R.id.expense_icon_view);
        dateValueTxtView = view.findViewById(R.id.date_value_txt_view);
        doneBtn = view.findViewById(R.id.done_btn);

        formatter = new SimpleDateFormat("dd-MMM", Locale.ENGLISH);

        // Create the adapter and set it to the AutoCompleteTextView
        adapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()), android.R.layout.simple_list_item_1, filteredCategories);
        categoryAutoCompleteTxtView.setAdapter(adapter);

        if (expenseEntry != null) {
            categoryAutoCompleteTxtView.setText(expenseEntry.getCategory());
            descriptionEditTxt.setText(expenseEntry.getDescription());
            valueEditTxt.setText(expenseEntry.getValue());
            iconView.setIcon(expenseEntry.getIconId());
            dateValueTxtView.setText(expenseEntry.getDate());
        } else {
            Date mDate = Calendar.getInstance().getTime();
            String today = formatter.format(mDate);
            dateValueTxtView.setText(today);
        }

        dateValueTxtView.setOnClickListener(this);
        iconView.setOnClickListener(this);
        doneBtn.setOnClickListener(this);
    }

    private void setupViewModel() {
        // Get a new or existing ViewModel from the ViewModelProvider.
        mMyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MyViewModel.class);

        // Observe the budget entries and update the adapter
        mMyViewModel.getBudgets().observe(this, new Observer<List<BudgetEntry>>() {
            @Override
            public void onChanged(@Nullable List<BudgetEntry> budgetEntries) {
                if (budgetEntries != null)
                    for (int i = 0; i < budgetEntries.size(); i++) {
                        categories.add(budgetEntries.get(i).getCategory());
                    }
                // remove duplicates
                setOfCategories.addAll(categories);
                filteredCategories.addAll(setOfCategories);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private Boolean fieldsAreFilled() {
        if (category.isEmpty()) {
            Toast.makeText(getContext(), "You must create a category first..", Toast.LENGTH_SHORT).show();
            return false;
        } else if (value.isEmpty()) {
            Toast.makeText(getContext(), "You must put in a value..", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == iconView) {
            IconDialog iconDialog = new IconDialog();
            iconDialog.setMaxSelection(1, false);
            iconDialog.setTargetFragment(FragmentNewExpense.this, 0);
            iconDialog.show(Objects.requireNonNull(getFragmentManager()), "icon_dialog");
        } else if (v == dateValueTxtView) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(Objects.requireNonNull(getContext()),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            Calendar newCal = Calendar.getInstance();
                            newCal.set(Calendar.YEAR, year);
                            newCal.set(Calendar.MONTH, monthOfYear);
                            newCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            Date newDate = newCal.getTime();
                            dateValueTxtView.setText(formatter.format(newDate));

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } else if (v == doneBtn) {
            category = categoryAutoCompleteTxtView.getText().toString();
            description = descriptionEditTxt.getText().toString();
            value = valueEditTxt.getText().toString();
            date = dateValueTxtView.getText().toString();
            if (selectedIconId == -1){
                selectedIconId = expenseEntry.getIconId();
            }
            if (fieldsAreFilled()) {
                if (expenseEntry != null) {
                    //update expenseEntry in database and return to FragmentExpenses
                    expenseEntry.setCategory(category);
                    expenseEntry.setDescription(description);
                    expenseEntry.setValue(value);
                    expenseEntry.setIconId(selectedIconId);
                    expenseEntry.setDate(date);
                    mMyViewModel.updateExpense(expenseEntry);
                } else {
                    //save expenseEntry in database and return to FragmentExpenses
                    newExpenseEntry = new ExpenseEntry(category, description, value, selectedIconId, date);
                    mMyViewModel.insertExpense(newExpenseEntry);
                }

                onSaveExpenseSelectionInterface.onSaveExpenseItemSelected();
            }
        }
    }

    @Override
    public void onIconDialogIconsSelected(IconDialog dialog, @NonNull Icon[] icons) {
        iconView.setIcon(icons[0]);
        //get icon id to save it in database
        selectedIconId = icons[0].getId();
    }
}

