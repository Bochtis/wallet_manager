package com.example.dimitris.walletmanager.feature.expenses;

public interface OnAddExpenseSelectionInterface {
    void onAddExpenseItemSelected();
}
