package com.example.dimitris.walletmanager.feature.budget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.prefs.PreferencesHelper;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.example.dimitris.walletmanager.utils.Utils.formatValue;
import static com.example.dimitris.walletmanager.utils.Utils.toCamelCase;

public class BudgetsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private Context context;
    private List<BudgetEntry> budgetEntries;
    private OnActionClickedInterface onActionClickedInterface;
    private PreferencesHelper prefs;
    private HashMap<String, Double> hashMap;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));

    BudgetsRecyclerAdapter(Context context, OnActionClickedInterface onActionClickedInterface) {
        this.context = context;
        this.onActionClickedInterface = onActionClickedInterface;
        prefs = new PreferencesHelper(context);
    }

    public interface OnActionClickedInterface {
        void onEditBudgetClicked(BudgetEntry budget);

        void onDeleteBudgetClicked(BudgetEntry budget);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_header, parent, false);
            return new MyHeaderViewHolder(view);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_item, parent, false);
            return new MyViewHolder(view);
        }

        throw new RuntimeException("There is no type that matches the type: " + viewType + ", make sure you're using types correctly.");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyHeaderViewHolder) {
            ((MyHeaderViewHolder) holder).bind();
        } else if (holder instanceof MyViewHolder) {
            ((MyViewHolder) holder).bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return (budgetEntries != null ? budgetEntries.size() : 0) + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private BudgetEntry getItem(int position) {
        return budgetEntries.get(position - 1);
    }

    void setBudgetEntries(List<BudgetEntry> budgetEntries) {
        this.budgetEntries = budgetEntries;
        notifyDataSetChanged();
    }

    void setExpensesPerBudget(HashMap<String, Double> hashMap) {
        this.hashMap = hashMap;
    }

    class MyHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView budgetTxtView, saveTxtView;
        EditText budgetEditTxt;

        MyHeaderViewHolder(View itemView) {
            super(itemView);
            budgetEditTxt = itemView.findViewById(R.id.budget_edit_txt);
            budgetTxtView = itemView.findViewById(R.id.budget_txt_view);
            saveTxtView = itemView.findViewById(R.id.save_txt_view);
        }

        void bind() {
            if (!prefs.getBudget().isEmpty()) {
                budgetEditTxt.setVisibility(View.INVISIBLE);
                budgetTxtView.setVisibility(View.VISIBLE);
                budgetTxtView.setText(formatValue(prefs.getBudget()));
                saveTxtView.setText(context.getString(R.string.fragment_budget_edit));
            } else {
                budgetEditTxt.setVisibility(View.VISIBLE);
                budgetTxtView.setVisibility(View.GONE);
                saveTxtView.setText(context.getString(R.string.fragment_budget_save));
            }

            saveTxtView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (saveTxtView.getText().toString().equals(context.getString(R.string.fragment_budget_save))) {
                        if (budgetEditTxt.getText().toString().isEmpty()) {
                            budgetEditTxt.setError("Value mustn't be empty");
                        } else {
                            //save budget at preferences!!!
                            prefs.setBudget(Double.valueOf(budgetEditTxt.getText().toString()).toString());
                            budgetEditTxt.setVisibility(View.INVISIBLE);
                            budgetTxtView.setVisibility(View.VISIBLE);
                            budgetTxtView.setText(formatValue(Double.valueOf(budgetEditTxt.getText().toString()).toString()));
                            saveTxtView.setText(context.getString(R.string.fragment_budget_edit));
                        }
                    } else {
                        budgetTxtView.setVisibility(View.GONE);
                        budgetEditTxt.setVisibility(View.VISIBLE);
                        budgetEditTxt.setText("");
                        budgetEditTxt.requestFocus();
                        saveTxtView.setText(context.getString(R.string.fragment_budget_save));
                    }
                }
            });
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView categoryTxtView, budgetValueTxtView, expensesValueTxtView, editBudgetTxtView, deleteBudgetTxtView;
        ProgressBar progressBar;

        MyViewHolder(View itemView) {
            super(itemView);
            categoryTxtView = itemView.findViewById(R.id.category_txt_view);
            budgetValueTxtView = itemView.findViewById(R.id.budget_value_txt_view);
            expensesValueTxtView = itemView.findViewById(R.id.expenses_value_txt_view);
            progressBar = itemView.findViewById(R.id.progress_bar);
            editBudgetTxtView = itemView.findViewById(R.id.edit_budget_txt_view);
            deleteBudgetTxtView = itemView.findViewById(R.id.delete_budget_txt_view);
        }

        void bind(int position) {
            BudgetEntry budgetEntry = getItem(position);

            double budgetValue = Double.valueOf(budgetEntry.getValue());
            //noinspection ConstantConditions
            double expenseValue = (hashMap != null && hashMap.containsKey(budgetEntry.getCategory()) ? hashMap.get(budgetEntry.getCategory()) : 0);
            double progress = expenseValue * 100 / budgetValue;

            categoryTxtView.setText(toCamelCase(budgetEntry.getCategory()));
            budgetValueTxtView.setText(formatValue(budgetEntry.getValue()));
            expensesValueTxtView.setText(formatValue(Double.toString(expenseValue)));
            progressBar.setProgress((int) progress);

            editBudgetTxtView.setOnClickListener(this);
            deleteBudgetTxtView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.edit_budget_txt_view:
                    onActionClickedInterface.onEditBudgetClicked(getItem(getAdapterPosition()));
                    break;
                case R.id.delete_budget_txt_view:
                    onActionClickedInterface.onDeleteBudgetClicked(getItem(getAdapterPosition()));
                    break;
            }
        }
    }
}
