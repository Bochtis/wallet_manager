package com.example.dimitris.walletmanager.feature.expenses;

public interface OnExpenseItemSelectionInterface<T> {
    void onExpenseItemSelected(T item);
}
