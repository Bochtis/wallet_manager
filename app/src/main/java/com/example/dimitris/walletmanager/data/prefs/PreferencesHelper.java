package com.example.dimitris.walletmanager.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.dimitris.walletmanager.utils.Extras;
import com.example.dimitris.walletmanager.utils.ObjectSerializer;

import java.io.IOException;
import java.util.ArrayList;

public class PreferencesHelper {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public PreferencesHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(Extras.PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setFirstLaunchStatus(boolean firstLaunch) {
        editor = sharedPreferences.edit();
        editor.putBoolean(Extras.FIRST_LAUNCH, firstLaunch);
        editor.apply();
    }

    public boolean getFirstLaunchStatus() {
        return sharedPreferences.getBoolean(Extras.FIRST_LAUNCH, true);
    }

    public void setMonthChangedStatus(boolean monthChangedStatus) {
        editor = sharedPreferences.edit();
        editor.putBoolean(Extras.MONTH_CHANGED, monthChangedStatus);
        editor.apply();
    }

    public boolean getMonthChangedStatus() {
        return sharedPreferences.getBoolean(Extras.MONTH_CHANGED, false);
    }

    public void setLatestLaunchTimeInMillis(long millis) {
        editor = sharedPreferences.edit();
        editor.putLong(Extras.TIME, millis);
        editor.apply();
    }

    public long getLatestLaunchTimeInMillis() {
        return sharedPreferences.getLong(Extras.TIME, 0);
    }

    public void setBudget(String budget) {
        editor = sharedPreferences.edit();
        editor.putString(Extras.BUDGET, budget);
        editor.apply();
    }

    public String getBudget() {
        return sharedPreferences.getString(Extras.BUDGET, "0.0");
    }

    public void addColors(ArrayList<Integer> colors) {
        editor = sharedPreferences.edit();
        try {
            editor.putString(Extras.COLORS, ObjectSerializer.serialize(colors));
        } catch (IOException e) {
            e.printStackTrace();
        }
        editor.apply();
    }

    public ArrayList<Integer> getColors() {
        try {
            //noinspection unchecked
            return (ArrayList<Integer>) ObjectSerializer.deserialize(sharedPreferences.getString(Extras.COLORS, null));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
