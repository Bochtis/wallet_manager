package com.example.dimitris.walletmanager.feature.budget;

public interface OnEditBudgetSelectionInterface<T> {
    void onEditBudgetSelected(T budget);
}
