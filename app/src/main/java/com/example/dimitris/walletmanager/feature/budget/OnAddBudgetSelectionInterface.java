package com.example.dimitris.walletmanager.feature.budget;

public interface OnAddBudgetSelectionInterface {
    void onAddBudgetItemSelected();
}
