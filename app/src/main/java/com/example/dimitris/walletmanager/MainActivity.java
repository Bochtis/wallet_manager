package com.example.dimitris.walletmanager;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.data.prefs.PreferencesHelper;
import com.example.dimitris.walletmanager.feature.budget.FragmentBudget;
import com.example.dimitris.walletmanager.feature.budget.FragmentNewBudget;
import com.example.dimitris.walletmanager.feature.budget.OnAddBudgetSelectionInterface;
import com.example.dimitris.walletmanager.feature.budget.OnEditBudgetSelectionInterface;
import com.example.dimitris.walletmanager.feature.budget.OnSaveCategorySelectionInterface;
import com.example.dimitris.walletmanager.feature.expenses.FragmentExpenses;
import com.example.dimitris.walletmanager.feature.expenses.FragmentNewExpense;
import com.example.dimitris.walletmanager.feature.expenses.OnAddExpenseSelectionInterface;
import com.example.dimitris.walletmanager.feature.expenses.OnExpenseItemSelectionInterface;
import com.example.dimitris.walletmanager.feature.expenses.OnSaveExpenseSelectionInterface;
import com.example.dimitris.walletmanager.feature.overview.FragmentHome;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;
import java.util.Stack;

public class MainActivity extends AppCompatActivity implements OnAddExpenseSelectionInterface,
        OnSaveExpenseSelectionInterface, OnExpenseItemSelectionInterface<ExpenseEntry>, OnAddBudgetSelectionInterface,
        OnEditBudgetSelectionInterface<BudgetEntry>, OnSaveCategorySelectionInterface {

    FrameLayout container;
    BottomNavigationView bottomNavigationView;

    private PreferencesHelper prefs;
    private HashMap<String, Stack<Fragment>> mStacks;
    public static final String TAB_HOME = "tab_home";
    public static final String TAB_EXPENSES = "tab_expenses";
    public static final String TAB_BUDGET = "tab_budget";

    private String mCurrentTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar toolbar = getSupportActionBar();
        container = findViewById(R.id.container);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home:
                        selectedTab(TAB_HOME);
                        return true;
                    case R.id.expenses:
                        selectedTab(TAB_EXPENSES);
                        return true;
                    case R.id.budget:
                        selectedTab(TAB_BUDGET);
                        return true;
                }
                return false;
            }
        });
        bottomNavigationView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {

            }
        });

        mStacks = new HashMap<>();
        mStacks.put(TAB_HOME, new Stack<Fragment>());
        mStacks.put(TAB_EXPENSES, new Stack<Fragment>());
        mStacks.put(TAB_BUDGET, new Stack<Fragment>());

        //launch home
        assert toolbar != null;
        toolbar.setTitle(getString(R.string.action_bar_title));
        loadMenuFragment(TAB_HOME, new FragmentHome(), true);

        //setup tap target views for the first launch
        prefs = new PreferencesHelper(this);
        if (prefs.getFirstLaunchStatus()) {
            setupTargetViews();
            saveTimeOfFirstLaunch();
            prefs.setFirstLaunchStatus(false);
        }else {
            prefs.setMonthChangedStatus(monthHasChanged());
            // update latest launch time
            prefs.setLatestLaunchTimeInMillis(Calendar.getInstance().getTimeInMillis());
        }
    }

    private void saveTimeOfFirstLaunch(){
        //save current month to prefs
        Calendar calendar = Calendar.getInstance();
        prefs.setLatestLaunchTimeInMillis(calendar.getTimeInMillis());
    }

    private void setupTargetViews() {
        new TapTargetSequence(this)
                .targets(
                        TapTarget.forView(findViewById(R.id.home), "This is your home screen", "Here you can see your wallet status!")
                                .outerCircleColor(R.color.tap_target_background)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.tap_target_secondary_background)
                                .titleTextSize(24)
                                .titleTextColor(R.color.tap_target_primary_text)
                                .descriptionTextSize(20)
                                .descriptionTextColor(R.color.tap_target_secondary_text)
                                .textTypeface(Typeface.SANS_SERIF)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(false)
                                .transparentTarget(false)
                                .targetRadius(70),
                        TapTarget.forView(findViewById(R.id.expenses), "This is the expenses screen", "Here you can see your current expenses and set new ones!")
                                .outerCircleColor(R.color.tap_target_background)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.tap_target_secondary_background)
                                .titleTextSize(24)
                                .titleTextColor(R.color.tap_target_primary_text)
                                .descriptionTextSize(20)
                                .descriptionTextColor(R.color.tap_target_secondary_text)
                                .textTypeface(Typeface.SANS_SERIF)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(false)
                                .transparentTarget(false)
                                .targetRadius(70),
                        TapTarget.forView(findViewById(R.id.budget), "This is the budget screen", "Here you can set your total and individual budgets!")
                                .outerCircleColor(R.color.tap_target_background)
                                .outerCircleAlpha(0.96f)
                                .targetCircleColor(R.color.tap_target_secondary_background)
                                .titleTextSize(24)
                                .titleTextColor(R.color.tap_target_primary_text)
                                .descriptionTextSize(20)
                                .descriptionTextColor(R.color.tap_target_secondary_text)
                                .textTypeface(Typeface.SANS_SERIF)
                                .drawShadow(true)
                                .cancelable(false)
                                .tintTarget(false)
                                .transparentTarget(false)
                                .targetRadius(70)
                ).listener(new TapTargetSequence.Listener() {
            @Override
            public void onSequenceFinish() {

            }

            @Override
            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

            }

            @Override
            public void onSequenceCanceled(TapTarget lastTarget) {

            }
        }).start();
    }

    private boolean monthHasChanged(){
        Calendar lastTimeCal = Calendar.getInstance();
        lastTimeCal.setTimeInMillis(prefs.getLatestLaunchTimeInMillis());
        Calendar newCal = Calendar.getInstance();

        return ((newCal.get(Calendar.YEAR) == lastTimeCal.get(Calendar.YEAR)) && (newCal.get(Calendar.MONTH) > lastTimeCal.get(Calendar.MONTH))) ||
                (newCal.get(Calendar.YEAR) > lastTimeCal.get(Calendar.YEAR));
    }

    public void selectedTab(String tabId) {
        mCurrentTab = tabId;
        Stack<Fragment> selectedStack = mStacks.get(tabId);

        if (selectedStack != null)
            if (selectedStack.size() == 0) {
                /*
                 *    First time this tab is selected. So add first fragment of that tab.
                 *    Don't need animation, so that argument is false.
                 *    We are adding a new fragment which is not present in stack. So add to stack is true.
                 */
                switch (tabId) {
                    case TAB_HOME:
                        loadMenuFragment(tabId, new FragmentHome(), true);
                        break;
                    case TAB_EXPENSES:
                        loadMenuFragment(tabId, new FragmentExpenses(), true);
                        break;
                    case TAB_BUDGET:
                        loadMenuFragment(tabId, new FragmentBudget(), true);
                        break;
                }
            } else {
                /*
                 *    We are switching tabs, and target tab already has at least one fragment.
                 *    If it has more than one fragments, clear it's stack.
                 *    No need of animation, no need of stack pushing. Just show the target fragment.
                 */
                int stackSize = selectedStack.size();
                if (stackSize > 1)
                    selectedStack.subList(1, stackSize).clear();
                loadMenuFragment(tabId, selectedStack.get(0), false);
            }
    }

    private void loadMenuFragment(String tag, Fragment fragment, boolean shouldAdd) {
        if (shouldAdd)
            Objects.requireNonNull(mStacks.get(tag)).push(fragment);

        // Add the new tab fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    private void loadFragment(Fragment fragment) {
        Objects.requireNonNull(mStacks.get(mCurrentTab)).push(fragment);

        // Add the new tab fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }

    void showDialog(BudgetEntry budgetEntry) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = FragmentNewBudget.newInstance(budgetEntry);
        newFragment.show(ft, "dialog");
    }

    private void popFragment() {
        Stack<Fragment> currentStack = mStacks.get(mCurrentTab);
        /*
         *    Select the second last fragment in current tab's stack..
         *    which will be shown after the fragment transaction given below
         */
        if (currentStack != null) {
            Fragment fragment = currentStack.elementAt(currentStack.size() - 2);

            /*pop current fragment from stack.. */
            currentStack.pop();

            /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (Objects.requireNonNull(mStacks.get(mCurrentTab)).size() == 1) {
            // We are already showing first fragment of current tab, so when back pressed...
            if (mCurrentTab.equals(TAB_HOME)) {
                //we will finish this activity if the current tab is the home tab
                finish();
            } else {
                //otherwise we will go to the home tab
                bottomNavigationView.setSelectedItemId(R.id.home);
            }
            return;
        }

        /* Goto previous fragment in navigation stack of this tab */
        popFragment();
    }

    @Override
    public void onAddExpenseItemSelected() {
        loadFragment(new FragmentNewExpense());
    }

    @Override
    public void onSaveExpenseItemSelected() {
        goBack();
    }

    @Override
    public void onExpenseItemSelected(ExpenseEntry item) {
        loadFragment(FragmentNewExpense.newInstance(item));
    }

    @Override
    public void onAddBudgetItemSelected() {
        showDialog(null);
    }

    @Override
    public void onEditBudgetSelected(BudgetEntry budget) {
        showDialog(budget);
    }

    @Override
    public void onSaveCategoryItemSelected() {
        goBack();
    }
}
