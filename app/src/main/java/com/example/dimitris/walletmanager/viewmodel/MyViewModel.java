package com.example.dimitris.walletmanager.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.data.repository.MyRepository;

import java.util.List;

public class MyViewModel extends AndroidViewModel {

    private LiveData<List<ExpenseEntry>> mExpenses;
    private LiveData<List<BudgetEntry>> mBudgets;
    private MyRepository mRepository;

    public MyViewModel(Application application) {
        super(application);
        mRepository = new MyRepository(application);
        mExpenses = mRepository.getExpenses();
        mBudgets = mRepository.getBudgets();
    }

    public LiveData<List<ExpenseEntry>> getExpenses() {
        return mExpenses;
    }

    public void insertExpense(ExpenseEntry expenseEntry) {
        mRepository.insertExpense(expenseEntry);
    }

    public void updateExpense(ExpenseEntry expenseEntry) {
        mRepository.updateExpense(expenseEntry);
    }

    public void deleteExpense(ExpenseEntry expenseEntry) {
        mRepository.deleteExpense(expenseEntry);
    }

    public void clearExpenses() {
        mRepository.clearExpenses();
    }

    public LiveData<List<BudgetEntry>> getBudgets() {
        return mBudgets;
    }

    public void insertBudget(BudgetEntry budgetEntry) {
        mRepository.insertBudget(budgetEntry);
    }

    public void updateBudget(BudgetEntry budgetEntry) {
        mRepository.updateBudget(budgetEntry);
    }

    public void deleteBudget(BudgetEntry budgetEntry) {
        mRepository.deleteBudget(budgetEntry);
    }

    public void clearBudgets() {
        mRepository.clearBudgets();
    }
}
