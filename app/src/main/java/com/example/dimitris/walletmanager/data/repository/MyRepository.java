package com.example.dimitris.walletmanager.data.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.data.db.MyDao;
import com.example.dimitris.walletmanager.data.db.MyRoomDatabase;
import com.example.dimitris.walletmanager.data.executors.MyExecutors;

import java.util.List;

public class MyRepository {

    private MyDao myDao;
    private LiveData<List<ExpenseEntry>> mExpenses;
    private LiveData<List<BudgetEntry>> mBudgets;

    // In order to unit test the MyRepository, we have to remove the Application dependency.
    // This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    public MyRepository(Application application) {
        MyRoomDatabase db = MyRoomDatabase.getDatabase(application);
        myDao = db.myDao();
        mExpenses = myDao.getExpenses();
        mBudgets = myDao.getBudgets();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<ExpenseEntry>> getExpenses() {
        return mExpenses;
    }

    // We must call this on a non-UI thread or the app will crash.
    // Like this, Room ensures that we're not doing any long running operations on the main
    // thread, blocking the UI.
    public void insertExpense(final ExpenseEntry expenseEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.insertExpense(expenseEntry);
            }
        });
    }

    public void updateExpense(final ExpenseEntry expenseEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.updateExpense(expenseEntry);
            }
        });
    }

    public void deleteExpense(final ExpenseEntry expenseEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.deleteExpense(expenseEntry);
            }
        });
    }

    public void clearExpenses() {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.clearExpenses();
            }
        });
    }

    public LiveData<List<BudgetEntry>> getBudgets() {
        return mBudgets;
    }

    public void insertBudget(final BudgetEntry budgetEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.insertBudget(budgetEntry);
            }
        });
    }

    public void updateBudget(final BudgetEntry budgetEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.updateBudget(budgetEntry);
            }
        });
    }

    public void deleteBudget(final BudgetEntry budgetEntry) {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.deleteBudget(budgetEntry);
            }
        });
    }

    public void clearBudgets() {
        MyExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                myDao.clearBudgets();
            }
        });
    }
}
