package com.example.dimitris.walletmanager.feature.expenses;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;
import java.util.List;

import static com.example.dimitris.walletmanager.utils.Utils.formatValue;
import static com.example.dimitris.walletmanager.utils.Utils.toCamelCase;

public class ExpensesRecyclerAdapter extends RecyclerView.Adapter<ExpensesRecyclerAdapter.MyViewHolder> {

    private List<ExpenseEntry> expenses = new ArrayList<>();
    private ExpensesInterface expensesInterface;

    ExpensesRecyclerAdapter(ExpensesInterface expensesInterface) {
        this.expensesInterface = expensesInterface;
    }

    public interface ExpensesInterface {
        void onExpenseItemClicked(ExpenseEntry item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expense_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExpensesRecyclerAdapter.MyViewHolder holder, int position) {
        holder.bind(position);
    }

    public void setExpenses(List<ExpenseEntry> expenses) {
        this.expenses = expenses;
        notifyDataSetChanged();
    }

    void removeExpense(int position) {
        expenses.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    ExpenseEntry getExpense(int position) {
        return expenses.get(position);
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout containerFrame;
        public ConstraintLayout foregroundConstraint;
        private TextView category, description, date, value;
        private IconView icon;

        MyViewHolder(View itemView) {
            super(itemView);
            containerFrame = itemView.findViewById(R.id.container);
            foregroundConstraint = itemView.findViewById(R.id.foreground_constraint);
            category = itemView.findViewById(R.id.category_txt_view);
            description = itemView.findViewById(R.id.description_txt_view);
            date = itemView.findViewById(R.id.date_txt_view);
            value = itemView.findViewById(R.id.value_txt_view);
            icon = itemView.findViewById(R.id.icon_view);
        }

        void bind(int position) {
            final ExpenseEntry expenseEntry = expenses.get(position);
            category.setText(toCamelCase(expenseEntry.getCategory()));
            description.setText(expenseEntry.getDescription());
            date.setText(expenseEntry.getDate());
            value.setText(formatValue(Double.valueOf(expenseEntry.getValue()).toString()));
            icon.setIcon(expenseEntry.getIconId());
            containerFrame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expensesInterface.onExpenseItemClicked(expenseEntry);
                }
            });
        }
    }
}
