package com.example.dimitris.walletmanager.feature.budget;

public interface OnSaveCategorySelectionInterface{
    void onSaveCategoryItemSelected();
}
