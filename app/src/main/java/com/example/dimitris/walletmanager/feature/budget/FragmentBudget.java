package com.example.dimitris.walletmanager.feature.budget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dimitris.walletmanager.R;
import com.example.dimitris.walletmanager.data.db.BudgetEntry;
import com.example.dimitris.walletmanager.data.db.ExpenseEntry;
import com.example.dimitris.walletmanager.utils.GridSpacingItemDecoration;
import com.example.dimitris.walletmanager.viewmodel.MyViewModel;
import com.melnykov.fab.FloatingActionButton;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class FragmentBudget extends Fragment implements View.OnClickListener, BudgetsRecyclerAdapter.OnActionClickedInterface {

    private TextView addBudgetTxtView;
    private RecyclerView budgetRecyclerView;

    private OnAddBudgetSelectionInterface onAddBudgetSelectionInterface;
    private OnEditBudgetSelectionInterface onEditBudgetSelectionInterface;
    private BudgetsRecyclerAdapter adapter;
    private HashMap<String, Double> expensesPerCategory = new HashMap<>();
    private MyViewModel mMyViewModel;

    public FragmentBudget() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("CommitPrefEdits")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget, container, false);

        setupViews(view);
        setupViewModel();

        return view;
    }

    private void setupViews(View view) {
        addBudgetTxtView = view.findViewById(R.id.add_budget_txt_view);
        budgetRecyclerView = view.findViewById(R.id.budget_recycler_view);
        FloatingActionButton newBudgetBtn = view.findViewById(R.id.new_budget_btn);
        newBudgetBtn.attachToRecyclerView(budgetRecyclerView);
        newBudgetBtn.setOnClickListener(this);

        setupRecycler();
    }

    private void setupRecycler() {
        adapter = new BudgetsRecyclerAdapter(getContext(), this);
        budgetRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        budgetRecyclerView.addItemDecoration(new GridSpacingItemDecoration(10));
        budgetRecyclerView.setAdapter(adapter);
    }

    private void setupViewModel() {
        // Get a new or existing ViewModel from the ViewModelProvider.
        mMyViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(MyViewModel.class);

        mMyViewModel.getBudgets().observe(this, new Observer<List<BudgetEntry>>() {
            @Override
            public void onChanged(@Nullable List<BudgetEntry> budgetEntries) {
                if (budgetEntries == null || budgetEntries.size() == 0) {
                    addBudgetTxtView.setVisibility(View.VISIBLE);
                } else {
                    addBudgetTxtView.setVisibility(View.GONE);
                }
                adapter.setBudgetEntries(budgetEntries);
            }
        });

        mMyViewModel.getExpenses().observe(this, new Observer<List<ExpenseEntry>>() {
            @Override
            public void onChanged(@Nullable List<ExpenseEntry> expenseEntries) {
                expensesPerCategory.clear();
                if (expenseEntries != null && expenseEntries.size() > 0) {
                    double expenseValue;
                    for (ExpenseEntry expenseEntry : expenseEntries) {
                        expenseValue = Double.valueOf(expenseEntry.getValue());
                        if (expensesPerCategory.get(expenseEntry.getCategory()) != null) {
                            //noinspection ConstantConditions
                            expensesPerCategory.put(expenseEntry.getCategory(), expensesPerCategory.get(expenseEntry.getCategory()) + expenseValue);
                        } else {
                            expensesPerCategory.put(expenseEntry.getCategory(), expenseValue);
                        }
                    }
                }

                if (!expensesPerCategory.isEmpty())
                    adapter.setExpensesPerBudget(expensesPerCategory);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddBudgetSelectionInterface) {
            onAddBudgetSelectionInterface = (OnAddBudgetSelectionInterface) context;
        }
        if (context instanceof OnEditBudgetSelectionInterface) {
            onEditBudgetSelectionInterface = (OnEditBudgetSelectionInterface) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onAddBudgetSelectionInterface = null;
        onEditBudgetSelectionInterface = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.new_budget_btn) {
            onAddBudgetSelectionInterface.onAddBudgetItemSelected();
        }
    }

    @Override
    public void onEditBudgetClicked(BudgetEntry budget) {
        onEditBudgetSelectionInterface.onEditBudgetSelected(budget);
    }

    @Override
    public void onDeleteBudgetClicked(BudgetEntry budget) {
        mMyViewModel.deleteBudget(budget);
    }
}
