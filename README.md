# Wallet Manager
An Android Application that helps you keep track of your wallet transactions every month.

Set your monthly budget, add your expenses, define your basic budget categories and control your wallet transactions.

# What is this repository for?
The purpose of this project is to get familiar with some very basic components of the Android Jetpack.

# Screenshots
## [Home](screenshots/HomeScreen.png) [Expenses](screenshots/ExpensesScreen.png) [Budget](screenshots/BudgetScreen.png)

# Libraries used
- [Architecture](https://developer.android.com/topic/libraries/architecture) - A collection of libraries that help you design testable and maintainable apps.
	- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Build data objects that notify views when the underlying database changes.
	- [Room](https://developer.android.com/topic/libraries/architecture/room) - Access your app's SQLite database with in-app objects and compile-time checks.
	- [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle) - Use the lifecycle-aware component [ViewModelProvider](https://developer.android.com/reference/androidx/lifecycle/ViewModelProvider).
	- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Store UI-related data that isn't destroyed on app rotations. Easily schedule asynchronous tasks for optimal execution.
- [UI](https://developer.android.com/guide/topics/ui) - Details on why and how to use UI Components in your apps - together or separate.
	- [Fragment](https://developer.android.com/guide/components/fragments) - A basic unit of composable UI.
	- [Layout](https://developer.android.com/guide/topics/ui/declaring-layout) - Lay out widgets using different algorithms.
		- [ConstraintLayout](https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout.html) - A ViewGroup that allows you to position and size widgets in a flexible way.
		- [CardView](https://developer.android.com/reference/androidx/cardview/widget/package-summary.html?hl=en) - A FrameLayout with a rounded corner background and shadow.
- [Material Design](https://www.material.io/develop/android/docs/getting-started/) - An adaptable system of guidelines, components, and tools that support the best practices of user interface design.
- Third party
	- [TapTargetView](https://github.com/KeepSafe/TapTargetView) - An implementation of tap targets from the Material Design guidelines for feature discovery.
	- [PieChart](https://github.com/PhilJay/MPAndroidChart) - An Android chart view/graph view library.
	- [FloatingActionButton](https://github.com/makovkastar/FloatingActionButton) - Android [floating action button](https://material.io/components/buttons-floating-action-button/) which reacts on scrolling events.
	- [IconDialog](https://github.com/maltaisn/icondialoglib) - Material icon picker dialog for Android
	